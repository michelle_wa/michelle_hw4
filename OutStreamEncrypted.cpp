#include <iostream>
#include "OutStreamEncrypted.h"
#include <string.h>

/*Set hist*/
OutStreamEncrypted::OutStreamEncrypted(int _hist)  
{
	this->_hist = _hist; 
}

/*d'tor*/
OutStreamEncrypted::~OutStreamEncrypted() { }

/*Encryption*/
OutStreamEncrypted& OutStreamEncrypted::operator<<(const char *str1)
{
	int len = strlen(str1) + 1;
	char* str2 = new char[len];

	for (int i = 0; i < len; i++)
	{
		if (str1[i] >= 32 && str1[i] <= 126)
		{
			if (str1[i] + this->_hist >= 126)
			{
				str2[i] = 126 - (str1[i] + this->_hist);

			}
			else
			{
				str2[i] = str1[i] + this->_hist;
			}
		}
		else
		{
			delete[] str2;
			return *this;
		}
	}
	delete[] str2;
	OutStream::operator<<(str2);
	return *this;
}
