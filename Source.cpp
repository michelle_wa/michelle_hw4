#include <iostream>
#include "OutStream.h"
#include "FileStream.h"
#include "OutStreamEncrypted.h"
#include "logger.h"


int main()
{
	OutStream os;
	char str_1[] = "I am the Doctor and I'm ";
	char str_2[] = " years old";
	os << (str_1);
	os << (1500);
	os << (str_2);
	os << (&endline);


	FileStream str1("Text.txt");
	str1 << (str_1);
	str1 << (1500);
	str1 << (str_2);
	str1 << endline;


	OutStreamEncrypted encryption(2);
	encryption << ("I am the Doctor and I'm 1500 years old");
	endline();

	Logger log1;
	Logger log2;
	log1 << "I am the Doctor and I'm " << 1500 << " years old" << endline;
	log2 << "I am the Doctor and I'm " << 1500 << " years old" << endline;

	system("pause");
	return 0;
}
