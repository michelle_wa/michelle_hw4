#pragma once

#include <iostream>
#include <string>
#define _CRT_SECURE_NO_WARNINGS

class OutStream
{
protected:
	FILE * _pFile;


public:
	OutStream();
	~OutStream();

	OutStream& operator<<(const char *str);
	OutStream& operator<<(int num);
	OutStream& operator<<(void(*pf)());
};

void endline();