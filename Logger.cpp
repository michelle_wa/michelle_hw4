#include "Logger.h"
#include <string>
#include <iostream>

unsigned int Logger::num = 1;


Logger::Logger() { _startLine = true; }

/*d'tor*/
Logger::~Logger() { }

/*Get SartLine*/
bool Logger::Get_SartLine()  { return this->_startLine; }


void Logger::setStartLine()
{
	if (this->_startLine == true)
	{
		printf(" LOG %d", num);
		num = num + 1;
	}
}

Logger& operator<<(Logger& l, const char *msg)
{
	l.setStartLine();
	l.os << (msg);
	if (msg[strlen(msg)] == '/n')
	{
		l._startLine = true;
	}
	else
	{
		l._startLine = false;
	}
	return l;
}

Logger& operator<<(Logger& l, int num)
{
	l.setStartLine();
	l.os << (num);
	return l;
}

Logger& operator<<(Logger& l, void(*pf)())
{
	pf();
	l._startLine = true;
	return l;
}